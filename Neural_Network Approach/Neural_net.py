
from sklearn.preprocessing import LabelEncoder
from keras.models import Sequential
from keras.layers import Dense
from keras.wrappers.scikit_learn import KerasClassifier
from keras.utils import np_utils
import numpy as np
from sklearn.pipeline import Pipeline
from keras.models import load_model
from sklearn import decomposition
import cv2
import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix

seed=10

np.random.seed(seed)

train_data=np.load('train_datamix.npz');
X=train_data['X']/255
X=X.reshape((995,40000))
y=train_data['y']

print(X.shape)
print(y.shape)

encoder = LabelEncoder()
encoder.fit(y)
encoded_Y = encoder.transform(y)
# convert integers to dummy variables (i.e. one hot encoded)
dummy_y = np_utils.to_categorical(encoded_Y)
#dummy_y = np.where(dummy_y==1)[1]


test_data = np.load('test_datamix.npz');
X_test = test_data['X']/255
X_test = X_test.reshape((480,40000))
y_test = test_data['y']

dummyones = np.ones(y_test.shape)

y_test = np.append(y_test,dummyones,axis=1)


#encoded_Y1 = encoder.transformy_test)
# convert integers to dummy variables (i.e. one hot encoded)
#dummy_y_test = np_utils.to_categorical(encoded_Y1)




#PCA

pca = decomposition.PCA(0.95)
pca.fit(X)
X = pca.transform(X)
X_test = pca.transform(X_test)

#pca2 = decomposition.PCA(0.9)
#pca2.fit(X_test)
#X_test = pca2.transform(X_test)


def base_model():
    model=Sequential()
    model.add(Dense(8, input_dim=397, activation='relu'))
    model.add(Dense(30, activation='relu'))
    model.add(Dense(30, activation='relu'))
    model.add(Dense(2, activation='sigmoid'))
    model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
    return model
    
    
model=base_model()
history = model.fit(X, dummy_y, epochs=50, batch_size=10)

scores = model.evaluate(X_test, y_test)
print("\n%s: %.2f%%" % (model.metrics_names[1], scores[1]*100))

plt.plot(history.history['acc'],'r')
plt.title('Model Accuracy')
plt.ylabel('Accuracy')
plt.xlabel('Epoch')
plt.show()


plt.plot(history.history['loss'],'r')
plt.title('Model Loss')
plt.ylabel('Loss')
plt.xlabel('Epoch')
plt.show()


