Name: Shivam Raval
Objective: To build a classifier to predict whether a given image is blurred.

Approach:
Crude approach using Laplacian kernel:
In this approach we take a single channel of an image (grayscale) and convolve it with the following 3 x 3 kernel of Laplacian and then take the variance.
If the variance falls below a pre-defined threshold, then the image is considered blurry; otherwise, the image is not blurry. The reason why this method works I mentioned here: https://www.pyimagesearch.com/2015/09/07/blur-detection-with-opencv/
The main task here is to decide the threshold for the better working of algorithm. For that,
Code:
laplacian_var.py gives the average value of variance of the three parts in training set.
The average variance for Naturally-Blurred images is = 83.7289223671
The average variance for Artificially-Blurred images is = 35.7596675002
The average variance for undistorted images is = 216.277538257
Using these values we can decide the threshold. It can be seen that the average variance for Naturally-Blurred images are higher as compares to Artifically-Blurred images so we have keep the threshold according to that.
Detect.py  
It gives the accuracy for the blurred images in EvaluationSet/DigitalBlurSet which turns out to be 95.625% and the threshold was kept at 110 and the average variance = 31.8723403621.

EvaluationSet/NaturalBlurSet Accuracy turns out to be 64.5% with threshold = 110 and the average variance is 127.227622871. It can be seen that the average variance is higher if we have kept threshold equals to 130 we could have got better accuracy. 


Using Shallow Neural Network:
Libraries used: Sklearn, Keras, Tensorflow (as keras use tensorflow as backend), Numpy, Matplotlib (for plotting accuracy and loss graph), Opencv

Codes:
train_database.py - This script generates labelled dataset to be passed to neural network. It goes inside the folder TrainingSet/Artificially-Blurred and TrainingSet/Naturally-Blurred/ reads the image and as all the images are not of the same size it resizes the images to 200x200 and assign label 0 to it. Label 0 for Blurred images and Label 1 for undistorted images. Similarly it goes inside TrainingSet/Undistorted , resizes the image and assign label 1 to it. A .npz file is generated in which X is data image and Y is the labels.

test_database.py - Working is similar to train_database.py

Neural_net.py - The data in .npz file is loaded and the labels are converted into one-hot encoding to be passed to the neural network. Principal component analysis is done to extract the features from the array of images. The model is trained with 50 epochs. The training accuracy is 98.49% and the testing accuracy is the 72.29%. 

