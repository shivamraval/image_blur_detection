import cv2
import os
import numpy as np

files = os.listdir(os.getcwd() + '/CERTH_ImageBlurDataset/TrainingSet/Undistorted/')

lap =0
count = 0
for f in files:
    img = cv2.imread(os.getcwd() + '/CERTH_ImageBlurDataset/TrainingSet/Undistorted/' + f,cv2.IMREAD_GRAYSCALE)
    fm = cv2.Laplacian(img,cv2.CV_64F).var()
    lap = fm + lap
    count = count + 1
    
print('Average varaince for Undistored')        
print(lap/count)

files = os.listdir(os.getcwd() + '/CERTH_ImageBlurDataset/TrainingSet/Naturally-Blurred/')

lap =0
count = 0
for f in files:
    img = cv2.imread(os.getcwd() + '/CERTH_ImageBlurDataset/TrainingSet/Naturally-Blurred/' + f,cv2.IMREAD_GRAYSCALE)
    fm = cv2.Laplacian(img,cv2.CV_64F).var()
    lap = fm + lap
    count = count + 1
    
print('Average varaince for Naturally-Blurred')    
print(lap/count)


files = os.listdir(os.getcwd() + '/CERTH_ImageBlurDataset/TrainingSet/Artificially-Blurred/')

lap =0
count = 0
for f in files:
    img = cv2.imread(os.getcwd() + '/CERTH_ImageBlurDataset/TrainingSet/Artificially-Blurred/' + f,cv2.IMREAD_GRAYSCALE)
    fm = cv2.Laplacian(img,cv2.CV_64F).var()
    lap = fm + lap
    count = count + 1
    
print('Average varaince for Artificially-Blurred')    
print(lap/count)
