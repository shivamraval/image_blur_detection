# -*- coding: utf-8 -*-
"""
Created on Sun May  6 21:25:39 2018

@author: raval
"""

import cv2
import os
import numpy as np

import numpy as np


files = os.listdir(os.getcwd() + '/CERTH_ImageBlurDataset/TrainingSet/Artificially-Blurred/')
IMG_SIZE=200

square_size = 200

data_img = []
label = []
for f in files:
    img = cv2.imread(os.getcwd() + '/CERTH_ImageBlurDataset/TrainingSet/Artificially-Blurred/' + f,cv2.IMREAD_GRAYSCALE)
    img = cv2.resize(img,(IMG_SIZE,IMG_SIZE))
    img = np.array(img)
    img=img.flatten()
    img=img.reshape((1,40000))
    temp_label = 0
    data_img.append((img))
    label.append((temp_label))

files = os.listdir(os.getcwd() + '/CERTH_ImageBlurDataset/TrainingSet/Naturally-Blurred/')


for f in files:
    img = cv2.imread(os.getcwd() + '/CERTH_ImageBlurDataset/TrainingSet/Naturally-Blurred/' + f,cv2.IMREAD_GRAYSCALE)
    img = cv2.resize(img,(IMG_SIZE,IMG_SIZE))
    img = np.array(img)
    img=img.flatten()
    img=img.reshape((1,40000))
    temp_label = 0
    data_img.append((img))
    label.append((temp_label))


files = os.listdir(os.getcwd() + '/CERTH_ImageBlurDataset/TrainingSet/Undistorted/')


for f in files:
    img = cv2.imread(os.getcwd() + '/CERTH_ImageBlurDataset/TrainingSet/Undistorted/' + f,cv2.IMREAD_GRAYSCALE)
    img = cv2.resize(img,(IMG_SIZE,IMG_SIZE))
    img = np.array(img)
    img=img.flatten()
    img=img.reshape((1,40000))
    temp_label = 1
    data_img.append((img))
    label.append((temp_label))


    
data_img = np.array(data_img)
data_img = data_img.reshape((995,40000))
label = np.array(label)
label = label.reshape((995,1))
np.savez('train_datamix.npz',X=data_img,y=label)