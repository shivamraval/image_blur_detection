import cv2
import os
import numpy as np


files = os.listdir(os.getcwd() + '/CERTH_ImageBlurDataset/EvaluationSet/DigitalBlurSet/')

lap =0
count = 0
count_blurr = 0
for f in files:
    img = cv2.imread(os.getcwd() + '/CERTH_ImageBlurDataset/EvaluationSet/DigitalBlurSet/' + f,cv2.IMREAD_GRAYSCALE)
    fm = cv2.Laplacian(img,cv2.CV_64F).var()
    lap = fm + lap
    count = count + 1
    if fm < 110:
        count_blurr = count_blurr + 1
    
    
print('Accuracy for Digital Blur Set')        
print(count_blurr/count)
print('Average Digital Blur Set')
print(lap/count)



files = os.listdir(os.getcwd() + '/CERTH_ImageBlurDataset/EvaluationSet/NaturalBlurSet/')

lap =0
count = 0
count_blurr = 0
for f in files:
    img = cv2.imread(os.getcwd() + '/CERTH_ImageBlurDataset/EvaluationSet/NaturalBlurSet/' + f,cv2.IMREAD_GRAYSCALE)
    fm = cv2.Laplacian(img,cv2.CV_64F).var()
    lap = fm + lap
    count = count + 1
    if fm < 110:
        count_blurr = count_blurr + 1
    
    
print('Accuracy for Natural Blur Set')        
print(count_blurr/count)
print('Average for Natural Blur Set')
print(lap/count)
